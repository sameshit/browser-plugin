//
//  FullScreenView.mm
//  FireBreath
//
//  Created by Oleg on 09.08.11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "FullScreenView.h"
#include "VideoLayer.h"

@implementation FullScreenView

- (id)initWithFrame:(NSRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        NSLog(@"COnstructor fsview called");
        layer = [VideoLayer new];
        layer.asynchronous = YES;// (FB::PluginWindowMac::DrawingModelInvalidatingCoreAnimation == wnd->getDrawingModel()) ? NO : YES;
        layer.autoresizingMask = kCALayerWidthSizable | kCALayerHeightSizable;
        layer.needsDisplayOnBoundsChange = YES;
        
        [self setLayer:layer];
        [self setWantsLayer:YES];        
    }
    
    return self;
}

- (void)SetVideoDecoder:(VideoDecoder *)is_val
{
    [layer SetVideoDecoder:is_val];
}

- (void)drawRect:(NSRect)dirtyRect
{
    // Drawing code here.
}

@end
