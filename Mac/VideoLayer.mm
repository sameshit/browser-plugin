//
//  VideoLayer.mm
//  FireBreath
//
//  Created by Oleg on 09.08.11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "VideoLayer.h"


@implementation VideoLayer

- (id) init {
    if ([super init]) {
        m_angle = 0;
        my_id = __id;
        __id +=1;
    }
    return self;
}

- (BOOL)canDrawInCGLContext:(CGLContextObj)glContext pixelFormat:(CGLPixelFormatObj)pixelFormat forLayerTime:(CFTimeInterval)timeInterval displayTime:(const CVTimeStamp *)timeStamp
{
	return YES;
}

- (CGLContextObj)copyCGLContextForPixelFormat:(CGLPixelFormatObj)pixelFormat
{
	CGLContextObj context = [super copyCGLContextForPixelFormat:pixelFormat];
    CVReturn err;
	
	
    
	err = CVOpenGLTextureCacheCreate(NULL,NULL,context,pixelFormat,NULL,&texture_cache);		
   	assert(kCVReturnSuccess == err);
	
	return context;
}


-(void)releaseCGLContext:(CGLContextObj)glContext
{	
	CVOpenGLTextureCacheRelease(texture_cache);
    [super releaseCGLContext:glContext];
}

- (void) SetVideoDecoder:(VideoDecoder*) is_val
{     
    is = is_val;
}


- (VideoDecoder *) GetVideoDecoder
{
	return is;
}



- (void)drawInCGLContext:(CGLContextObj)ctx pixelFormat:(CGLPixelFormatObj)pf forLayerTime:(CFTimeInterval)t displayTime:(const CVTimeStamp *)ts 
{		
    VideoDecoder *is = [self GetVideoDecoder];
    NSLog(@"drawInCGLContext(%d)", my_id);
	
	uint32_t w = CGRectGetWidth([self bounds]), h = CGRectGetHeight([self bounds]);
	uint8_t *data  = is->GetLastPicture()->bitmap;
	CVPixelBufferRef pixel_buffer; 
	CVReturn err;
	GLenum target;
	GLint name;
	GLfloat topLeft[2], topRight[2], bottomRight[2], bottomLeft[2];
	uint8_t *planes[4];
	size_t plane_w[4];
	size_t plane_h[4];
	size_t bytes_per_row[4];
	CVOpenGLTextureRef texture;
	
    
	
	if (!data)
		return;
    
    NSLog(@"%u %u",w,h);
    
	planes[0] = &data[0];
	planes[1] = &data[h*w/2];
	planes[2] = &data[h*w];
	planes[3] = &data[3*h*w/2];
    
	plane_w[0] = w;
	plane_w[1] = w;
	plane_w[2] = w;
	plane_w[3] = w;
	
	plane_h[0] = h;
	plane_h[1] = h;
	plane_h[2] = h;
	plane_h[3] = h;
	
	bytes_per_row[0] = 2*w;
	bytes_per_row[1] = 2*w;
	bytes_per_row[2] = 2*w;
	bytes_per_row[3] = 2*w;	
	
    //	CGLContextObj cgl_ctx = ctx; // CGLMacro.h
    //	CGLSetCurrentContext(ctx);
	
	
	
	glPushAttrib(GL_ALL_ATTRIB_BITS);
	glPushClientAttrib(GL_ALL_ATTRIB_BITS);	
	
	CVOpenGLTextureCacheFlush(texture_cache,0);
	
	err = CVPixelBufferCreateWithPlanarBytes(NULL,w,h,kCVPixelFormatType_422YpCbCr8,data,h*w*2,4,(void**)planes,plane_w,plane_h, bytes_per_row,NULL,NULL,NULL,&pixel_buffer);	
   	assert(kCVReturnSuccess == err);	
    
	err = CVOpenGLTextureCacheCreateTextureFromImage(NULL,texture_cache,pixel_buffer,NULL,&texture);
   	assert(kCVReturnSuccess == err);		
	
	
    glShadeModel(GL_SMOOTH);                // enable smooth shading
	assert(glGetError() == GL_NO_ERROR);
    glClearColor(0.0f, 0.0f, 0.0f, 0.0f);   // black background
	assert(glGetError() == GL_NO_ERROR);
    glClearDepth(1.0f);                     // depth buffer setup
	assert(glGetError() == GL_NO_ERROR);
    glEnable(GL_DEPTH_TEST);                // enable depth testing
	assert(glGetError() == GL_NO_ERROR);	
    glDepthFunc(GL_LEQUAL);                 // type of depth test to do
	assert(glGetError() == GL_NO_ERROR);
	
    glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
	assert(glGetError() == GL_NO_ERROR);
    
    glTexGeni(GL_S, GL_TEXTURE_GEN_MODE, GL_SPHERE_MAP);
	assert(glGetError() == GL_NO_ERROR);	
	
    
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	
    
	
	glMatrixMode(GL_TEXTURE);
	glLoadIdentity();
    
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
    
    
	target = CVOpenGLTextureGetTarget(texture); 
	name   = CVOpenGLTextureGetName(texture);      
	
    CVOpenGLTextureGetCleanTexCoords(texture, bottomLeft, bottomRight, topRight, topLeft);
	
	// get the texture coordinates for the part of the image that should be displayed
	glPushMatrix();		
	assert(glGetError() == GL_NO_ERROR);
    
	// bind the texture and draw the quad
	glEnable(target);
	assert(glGetError() == GL_NO_ERROR);
	
	glBindTexture(target, name);
	assert(glGetError() == GL_NO_ERROR);	
	
	glBegin(GL_QUADS);
	glTexCoord2fv(bottomLeft);  glVertex2i(-1, -1);
	glTexCoord2fv(topLeft);     glVertex2i(-1,  1);
	glTexCoord2fv(topRight);    glVertex2i( 1,  1);
	glTexCoord2fv(bottomRight); glVertex2i( 1, -1);
	glEnd();
	
	assert(glGetError() == GL_NO_ERROR);
	
	
	glDisable(target);
	assert(glGetError() == GL_NO_ERROR);	
	
	glPopMatrix();
	assert(glGetError() == GL_NO_ERROR);	
    
	glPopClientAttrib();	
	glPopAttrib();
    [super drawInCGLContext:ctx pixelFormat:pf forLayerTime:t displayTime:ts];
	
	CVPixelBufferRelease(pixel_buffer);	
	CVOpenGLTextureRelease(texture);
    
}
@end
