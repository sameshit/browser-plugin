/**********************************************************\

  Auto-generated wvplayerAPI.cpp

\**********************************************************/

#include "JSObject.h"
#include "variant_list.h"
#include "DOM/Document.h"

#include "wvplayerAPI.h"

///////////////////////////////////////////////////////////////////////////////
/// @fn wvplayerAPI::wvplayerAPI(const wvplayerPtr& plugin, const FB::BrowserHostPtr host)
///
/// @brief  Constructor for your JSAPI object.  You should register your methods, properties, and events
///         that should be accessible to Javascript from here.
///
/// @see FB::JSAPIAuto::registerMethod
/// @see FB::JSAPIAuto::registerProperty
/// @see FB::JSAPIAuto::registerEvent
///////////////////////////////////////////////////////////////////////////////
wvplayerAPI::wvplayerAPI(const wvplayerPtr& plugin, const FB::BrowserHostPtr& host) : m_plugin(plugin), m_host(host)
{
    registerMethod("echo",      make_method(this, &wvplayerAPI::echo));
    registerMethod("testEvent", make_method(this, &wvplayerAPI::testEvent));

    // Read-write property
    registerProperty("testString",
                     make_property(this,
                        &wvplayerAPI::get_testString,
                        &wvplayerAPI::set_testString));

    // Read-only property
    registerProperty("version",
                     make_property(this,
                        &wvplayerAPI::get_version));
}

///////////////////////////////////////////////////////////////////////////////
/// @fn wvplayerAPI::~wvplayerAPI()
///
/// @brief  Destructor.  Remember that this object will not be released until
///         the browser is done with it; this will almost definitely be after
///         the plugin is released.
///////////////////////////////////////////////////////////////////////////////
wvplayerAPI::~wvplayerAPI()
{
}

///////////////////////////////////////////////////////////////////////////////
/// @fn wvplayerPtr wvplayerAPI::getPlugin()
///
/// @brief  Gets a reference to the plugin that was passed in when the object
///         was created.  If the plugin has already been released then this
///         will throw a FB::script_error that will be translated into a
///         javascript exception in the page.
///////////////////////////////////////////////////////////////////////////////
wvplayerPtr wvplayerAPI::getPlugin()
{
    wvplayerPtr plugin(m_plugin.lock());
    if (!plugin) {
        throw FB::script_error("The plugin is invalid");
    }
    return plugin;
}



// Read/Write property testString
std::string wvplayerAPI::get_testString()
{
    return m_testString;
}
void wvplayerAPI::set_testString(const std::string& val)
{
    m_testString = val;
}

// Read-only property version
std::string wvplayerAPI::get_version()
{
    return "CURRENT_VERSION";
}

// Method echo
FB::variant wvplayerAPI::echo(const FB::variant& msg)
{
    static int n(0);
    fire_echo(msg, n++);
    return msg;
}

void wvplayerAPI::testEvent(const FB::variant& var)
{
    fire_fired(var, true, 1);
}

