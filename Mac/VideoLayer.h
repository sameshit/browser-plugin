//
//  VideoLayer.h
//  FireBreath
//
//  Created by Oleg on 09.08.11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//
#ifndef VIDEOLAYER_H
#define VIDEOLAYER_H

#import <Cocoa/Cocoa.h>
#include "../VideoDecoder.h"
#import <AppKit/AppKit.h>
#include "Mac/PluginWindowMacCA.h"

static int __id = 0;

@interface VideoLayer : CAOpenGLLayer 
{
    GLfloat m_angle;
    CVOpenGLTextureCacheRef texture_cache;
    VideoDecoder *is;
    int my_id;
}
@end

#endif