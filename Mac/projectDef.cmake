#/**********************************************************\ 
# Auto-generated Mac project definition file for the
# WebVision Player project
#\**********************************************************/

# Mac template platform definition CMake file
# Included from ../CMakeLists.txt

# remember that the current source dir is the project root; this file is in Mac/
file (GLOB PLATFORM RELATIVE ${CMAKE_CURRENT_SOURCE_DIR}
    Mac/[^.]*.cpp
    Mac/[^.]*.h
    Mac/[^.]*.cmake
    Mac/[^.]*.mm
    )

# use this to add preprocessor definitions
add_definitions(
    
)


SOURCE_GROUP(Mac FILES ${PLATFORM})

set (SOURCES
    ${SOURCES}
    ${PLATFORM}
    )

set(PLIST "Mac/bundle_template/Info.plist")
set(STRINGS "Mac/bundle_template/InfoPlist.strings")
set(LOCALIZED "Mac/bundle_template/Localized.r")
set(FFMPEGLIBS "/Users/void/easy.tv/ffmpeg/uni/lib")

include_directories(/Users/void/easy.tv/ffmpeg/i386/include)
include_directories(/Library/Frameworks/SDL.framework/Versions/A/Headers)

add_mac_plugin(${PROJECT_NAME} ${PLIST} ${STRINGS} ${LOCALIZED} SOURCES)

find_library(OPENGL_FRAMEWORK OpenGL)
find_library(QUARTZ_CORE_FRAMEWORK QuartzCore)
find_library(SDL_FRAMEWORK SDL)
find_library(SDL_MIXER_FRAMEWORK SDL_mixer)

# add library dependencies here; leave ${PLUGIN_INTERNAL_DEPS} there unless you know what you're doing!
target_link_libraries(${PROJECT_NAME}
    ${PLUGIN_INTERNAL_DEPS}
	${OPENGL_FRAMEWORK}
    ${QUARTZ_CORE_FRAMEWORK}
    ${SDL_FRAMEWORK}
    ${SDL_MIXER_FRAMEWORK}
    ${FFMPEGLIBS}/libavcodec.a
    ${FFMPEGLIBS}/libavformat.a
    ${FFMPEGLIBS}/libavutil.a
    ${FFMPEGLIBS}/libswscale.a
    -lz
    -lbz2
	"-read_only_relocs suppress"
    )
