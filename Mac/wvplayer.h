/**********************************************************\

  Auto-generated wvplayer.h

  This file contains the auto-generated main plugin object
  implementation for the WebVision Player project

\**********************************************************/
#ifndef H_wvplayerPLUGIN
#define H_wvplayerPLUGIN

#include "PluginWindow.h"
#include "PluginEvents/MouseEvents.h"
#include "PluginEvents/AttachedEvent.h"
#include "PluginEvents/MacEventCocoa.h"

#include "PluginCore.h"


class VideoDecoder;
class DecoderMac;

FB_FORWARD_PTR(wvplayer)
class wvplayer : public FB::PluginCore
{
public:
    static void StaticInitialize();
    static void StaticDeinitialize();

public:
    wvplayer();
    virtual ~wvplayer();
    static void Render(void *);
    VideoDecoder *GetVideoDecoder();
    void *GetFullScreenView();
    bool IsFullScreen();
public:
    void onPluginReady();
    void shutdown();
    virtual FB::JSAPIPtr createJSAPI();
    // If you want your plugin to always be windowless, set this to true
    // If you want your plugin to be optionally windowless based on the
    // value of the "windowless" param tag, remove this method or return
    // FB::PluginCore::isWindowless()
    virtual bool isWindowless() { return false; }

    BEGIN_PLUGIN_EVENT_MAP()
        EVENTTYPE_CASE(FB::MouseDownEvent, onMouseDown, FB::PluginWindow)
        EVENTTYPE_CASE(FB::MouseUpEvent, onMouseUp, FB::PluginWindow)
        EVENTTYPE_CASE(FB::MouseMoveEvent, onMouseMove, FB::PluginWindow)
        EVENTTYPE_CASE(FB::MouseMoveEvent, onMouseMove, FB::PluginWindow)
        EVENTTYPE_CASE(FB::AttachedEvent, onWindowAttached, FB::PluginWindow)
        EVENTTYPE_CASE(FB::DetachedEvent, onWindowDetached, FB::PluginWindow)
        EVENTTYPE_CASE(FB::ResizedEvent,onWindowResize,FB::PluginWindow)
    END_PLUGIN_EVENT_MAP()

    /** BEGIN EVENTDEF -- DON'T CHANGE THIS LINE **/
    virtual bool onMouseDown(FB::MouseDownEvent *evt, FB::PluginWindow *);
    virtual bool onMouseUp(FB::MouseUpEvent *evt, FB::PluginWindow *);
    virtual bool onMouseMove(FB::MouseMoveEvent *evt, FB::PluginWindow *);
    virtual bool onWindowAttached(FB::AttachedEvent *evt, FB::PluginWindow *);
    virtual bool onWindowDetached(FB::DetachedEvent *evt, FB::PluginWindow *);
    virtual bool onWindowResize(FB::ResizedEvent *evt,FB::PluginWindow*);
    /** END EVENTDEF -- DON'T CHANGE THIS LINE **/
private:
    void *m_layer;
    bool attached, resized;
    VideoDecoder *is;
    bool is_fullscreen;
    void *f_view;
    DecoderMac *decoder;
};


#endif

