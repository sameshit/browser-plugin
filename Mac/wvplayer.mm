/**********************************************************\

  Auto-generated wvplayer.cpp

  This file contains the auto-generated main plugin object
  implementation for the WebVision Player project

\**********************************************************/

#include "wvplayerAPI.h"

#include "wvplayer.h"
#include "VideoLayer.h"
#include <SDL_mixer/SDL_mixer.h>
#include "FullScreenView.h"
#include "DecoderMac.h"



void wvplayer::StaticInitialize()
{
	av_register_all();
	
	if(SDL_Init(SDL_INIT_AUDIO | SDL_INIT_TIMER)) {
		fprintf(stderr, "Could not initialize SDL - %s\n", SDL_GetError());
		exit(1);
	}
	
	if(Mix_OpenAudio(44100, AUDIO_S16SYS, 2, 1024)<0)
	{
		fprintf(stderr, "MIx_OpenAudio: %s\n", Mix_GetError());
		exit(1);
	}    
}

void wvplayer::StaticDeinitialize()
{
	Mix_CloseAudio();
	
	SDL_Quit();
}

wvplayer::wvplayer()
{
    decoder = NULL;
    is = new VideoDecoder;    
    attached = false;    
    resized  = false;
    is_fullscreen = false;
}

wvplayer::~wvplayer()
{
    delete is;
    
    if (m_layer) {
        [(CALayer*)m_layer release];
        m_layer = NULL;
    }
    
        
    releaseRootJSAPI();
    m_host->freeRetainedObjects();
}

void wvplayer::onPluginReady()
{

}

void wvplayer::shutdown()
{

}

FB::JSAPIPtr wvplayer::createJSAPI()
{
    return boost::make_shared<wvplayerAPI>(FB::ptr_cast<wvplayer>(shared_from_this()), m_host);
}

bool wvplayer::onMouseDown(FB::MouseDownEvent *evt, FB::PluginWindow *)
{ 
    NSLog (@"clicked");
//    is->SetSize(200,200);
 
    if (decoder != NULL)
        delete decoder;
    decoder = new DecoderMac;
    
    assert(decoder->OpenFile("/Users/void/Downloads/test.flv"));
    
    decoder->StartDecoding();
/*    is = new VideoDecoder;
    is->SetSize (200,200);
    is->SetRenderer(wvplayer::Render,this);    
    
    [(VideoLayer*)m_layer SetVideoDecoder:is];
    
    is->Play();
    /*
    NSRect bounds = [[NSScreen mainScreen] frame];
    
    NSWindow *mOffScreenWindow = [[NSWindow alloc] initWithContentRect:bounds
                                                   styleMask:NSBorderlessWindowMask
                                                     backing:NSBackingStoreBuffered
                                                       defer:YES];
    
    FullScreenView *view = [[FullScreenView alloc] initWithFrame:    [[NSScreen mainScreen] frame]];
    is->SetSize(bounds.size.width, bounds.size.height);
    [view SetVideoDecoder:is];
    NSDictionary* options = [NSDictionary
                             dictionaryWithObjectsAndKeys:
                             [NSNumber numberWithInt:kCGNormalWindowLevel],
                             NSFullScreenModeWindowLevel, nil];
    
    [mOffScreenWindow setContentView:view];
    
    [view enterFullScreenMode:[NSScreen mainScreen] withOptions:options];
    f_view = (void*) view;
    is_fullscreen = true;
    return false;*/
}

bool wvplayer::onMouseUp(FB::MouseUpEvent *evt, FB::PluginWindow *)
{
    return false;
}

bool wvplayer::onMouseMove(FB::MouseMoveEvent *evt, FB::PluginWindow *)
{
    return false;
}
bool wvplayer::onWindowAttached(FB::AttachedEvent *evt, FB::PluginWindow *win)
{
/*    FB::PluginWindowMac* wnd =  dynamic_cast<FB::PluginWindowMac*>(win);
    
    if (FB::PluginWindowMac::DrawingModelCoreAnimation == wnd->getDrawingModel() || FB::PluginWindowMac::DrawingModelInvalidatingCoreAnimation == wnd->getDrawingModel()) {
        // Setup CAOpenGL drawing.
        NSLog(@"a wss here");
        VideoLayer* layer = [VideoLayer new];
        layer.asynchronous = YES;// (FB::PluginWindowMac::DrawingModelInvalidatingCoreAnimation == wnd->getDrawingModel()) ? NO : YES;
        layer.autoresizingMask = kCALayerWidthSizable | kCALayerHeightSizable;
        layer.needsDisplayOnBoundsChange = YES;
        [layer SetVideoDecoder:is];
        m_layer = layer;
//        if (FB::PluginWindowMac::DrawingModelInvalidatingCoreAnimation == wnd->getDrawingModel())
//            wnd->StartAutoInvalidate(1.0/30.0);
        [(CALayer*) wnd->getDrawingPrimitive() addSublayer:layer];        
        
        is->SetRenderer(wvplayer::Render,this);
        
        attached = true;
        if (!is->IsPlaying() && resized)
            is->Play();
    }*/
    return false;
}

bool wvplayer::onWindowResize(FB::ResizedEvent *evt, FB::PluginWindow *wnd)
{
    /*
    if (is)
    {
        is->SetSize(wnd->getWindowWidth(), wnd->getWindowHeight());
        resized = true;
        if (!is->IsPlaying() && attached)
            is->Play();
    }*/
    return false;
}

bool wvplayer::onWindowDetached(FB::DetachedEvent *evt, FB::PluginWindow *)
{
    return false;
}

void wvplayer::Render(void *obj)
{
    wvplayer *player_ptr = (wvplayer*)obj;
    
    if (player_ptr->IsFullScreen())
    {
        NSLog(@"fscreen draw");
        [(FullScreenView*)player_ptr->GetFullScreenView() setNeedsDisplay:YES];
    }
    else 
    {
        NSLog(@"simple draw");
        player_ptr->GetWindow()->InvalidateWindow();
    }
}

VideoDecoder* wvplayer::GetVideoDecoder()
{
    return is;
}

void *wvplayer::GetFullScreenView()
{
    return f_view;
}

bool wvplayer::IsFullScreen()
{
    return is_fullscreen;
}

