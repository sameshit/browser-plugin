//
//  DecoderMac.h
//  FireBreath
//
//  Created by Oleg on 20.08.11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#ifndef FireBreath_DecoderMac_h
#define FireBreath_DecoderMac_h
#include "../Decoder.h"

class DecoderMac : public Decoder
{
public:
    int         QueuePicture(double);
    virtual ~DecoderMac();
};

#endif
