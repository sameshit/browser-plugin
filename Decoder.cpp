//
//  Decoder.cpp
//  FireBreath
//
//  Created by Oleg on 11.08.11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#include "Decoder.h"


Decoder::Decoder()
{
    format_ctx      = avformat_alloc_context();
    video_stream    = NULL;
    audio_stream    = NULL;
    mix_chunk       = NULL;

    memset(&audio_stream_data,0,sizeof(audio_stream_data));
    memset(&video_stream_data,0,sizeof(video_stream_data));
    
    video_stream_data.thread_id = NULL;
    video_stream_data.frame     = avcodec_alloc_frame();
    
    read_thread_id  = NULL;
    flush_pkt.data = (uint8_t*)flush_str;
    
    audio_queue     = new PacketQueue(&flush_pkt);
    video_queue     = new PacketQueue(&flush_pkt);
    exit            = false;
    
    audio_exit_mutex = SDL_CreateMutex();
}

Decoder::~Decoder()
{    
    SDL_LockMutex(audio_exit_mutex);
    Mix_HaltChannel(mix_channel);
    if (mix_chunk != NULL)
        Mix_FreeChunk(mix_chunk);
    exit = true;
    SDL_UnlockMutex(audio_exit_mutex);    
    SDL_DestroyMutex(audio_exit_mutex);
    
    avformat_free_context(format_ctx);
        
    if (read_thread_id != NULL)
        SDL_KillThread(read_thread_id);    
    
    av_free(video_stream_data.frame);
    
    delete audio_queue;
    delete video_queue;
}

bool Decoder::OpenFile(const char *filename)
{
    unsigned int i;
    
    if (avformat_open_input(&format_ctx,filename, NULL, NULL) < 0)
        return false;
    if (avformat_find_stream_info(format_ctx,NULL)<0)
        return false;
    
    for(i=0; i<format_ctx->nb_streams; i++) 
    {
        if      (video_stream == NULL && format_ctx->streams[i]->codec->codec_type==AVMEDIA_TYPE_VIDEO)
        {
            video_index  = i;
			video_stream = format_ctx->streams[i];
        }
		else if (audio_stream == NULL && format_ctx->streams[i]->codec->codec_type==AVMEDIA_TYPE_AUDIO)
        {
            audio_index  = i;
			audio_stream = format_ctx->streams[i];
        }
    }
    
    if (video_stream==NULL || audio_stream==NULL)
        return false;
    
    if (!OpenAudioStream() || !OpenVideoStream())
        return false;
    
    av_sync_type = SYNC_VIDEO_MASTER;
    
    return true;
}

bool Decoder::OpenAudioStream()
{
    AVCodecContext *codec_ctx = audio_stream->codec;
    AVCodec* audio_codec; // this variable may leak, but probably there is no reason to dealloc it

    audio_stream_data.buf_size          = 0;
    audio_stream_data.buf_index         = 0;
    audio_stream_data.tmp_pkt.data      = NULL;
    audio_stream_data.tmp_pkt.size      = 0;
    audio_stream_data.last_pkt.data     = NULL;
    audio_stream_data.last_pkt.size     = 0;
    audio_stream_data.clock             = 0; 
    audio_stream_data.diff_avg_coef     = exp(log(0.01 / AUDIO_DIFF_AVG_NB));
    audio_stream_data.diff_avg_count    = 0;
    audio_stream_data.diff_threshold    = 2.0 * 1024 / codec_ctx->sample_rate;
    audio_stream_data.diff_cum          = 0;
    
    mix_chunk   = Mix_QuickLoad_RAW(audio_buffer, 1024);
    if (mix_chunk == NULL)
        return false;
    
    mix_channel = Mix_PlayChannel(-1, mix_chunk, -1);  
		
	audio_codec = avcodec_find_decoder(codec_ctx->codec_id);
	if(!audio_codec || (avcodec_open2(codec_ctx, audio_codec, NULL) < 0))
        return false;
    
    
    
	
    return true;
}

bool Decoder::OpenVideoStream()
{    
    VideoStreamData *sdata = &video_stream_data;
    AVCodecContext *codec_ctx = video_stream->codec;
    AVCodec* video_codec; // this variable may leak, but probably there is no reason to dealloc it
        
    video_codec = avcodec_find_decoder(codec_ctx->codec_id);
    if (!video_codec || (avcodec_open2(codec_ctx,video_codec,NULL) < 0))
        return false;    
    
    codec_ctx->get_buffer = VideoGetBuffer;
    codec_ctx->release_buffer = VideoReleaseBuffer;
    codec_ctx->opaque = this;    
    
    sdata->frame_timer          = (double)av_gettime() / 1000000.0;
    sdata->frame_last_delay     = 40e-3;
    sdata->pts                  = av_gettime();    
    
    return true;
}

void Decoder::StartDecoding()
{
    read_thread_id              = SDL_CreateThread(Decoder::ReadThread,this);
    
    video_stream_data.thread_id = SDL_CreateThread(Decoder::VideoThread, this);
    
    Mix_RegisterEffect(mix_channel, Decoder::AudioCallback, NULL, this);
}

int Decoder::ReadThread(void *arg)
{
    Decoder             *decoder = (Decoder*)arg;
    AVPacket            pkt;
    AVFormatContext     *format_ctx = decoder->format_ctx;
    int ret;
    
    while (true)
    {
            //while (decoder->audio_queue->Size() > MAX_AUDIOQ_SIZE || decoder->video_queue->Size() > MAX_VIDEOQ_SIZE)
             //   SDL_Delay(10);
        
            ret = av_read_frame(format_ctx, &pkt);        
            if (ret < 0)
                break;
        
            if      (pkt.stream_index == decoder->audio_index)
                decoder->audio_queue->Put(&pkt);
            else if (pkt.stream_index == decoder->video_index)
                decoder->video_queue->Put(&pkt);
            else
                av_free_packet(&pkt);
    }
    
    return 1;
}

void Decoder::AudioCallback(int chan, void * _stream, int len, void * udata)
{
	Decoder *decoder = (Decoder *)udata;
	uint8_t *stream = (uint8_t *)_stream;
	int len1, audio_size;
	double pts;
    AudioStreamData *sdata = &decoder->audio_stream_data;
	
	while(len > 0) 
    {
        SDL_LockMutex(decoder->audio_exit_mutex);
        if (decoder->exit)
            return;
        
		if(sdata->buf_index >= sdata->buf_size) 
        {
			/* We have already sent all our data; get more */			
			audio_size = decoder->DecodeAudioFrame(&pts);
			if(audio_size < 0) 
            {
				/* If error, output silence */
				sdata->buf_size = 1024;
				memset(sdata->buffer, 0, sdata->buf_size);
			} 
            else 
            {
				audio_size = decoder->SynchronizeAudio(audio_size, pts);
				sdata->buf_size = audio_size;
			}
			sdata->buf_index = 0;
		}
		len1 = sdata->buf_size - sdata->buf_index;
		if(len1 > len)
			len1 = len;
		memcpy(stream, (uint8_t *)sdata->buffer + sdata->buf_index, len1);
		len -= len1;
		stream += len1;
		sdata->buf_index += len1;
        SDL_UnlockMutex(decoder->audio_exit_mutex);
    }
    
}

int Decoder::DecodeAudioFrame(double *out_pts)
{
	int len1, data_size =-1, n;
	double pts;
    AudioStreamData *sdata = &this->audio_stream_data;
    	    
    if (sdata->tmp_pkt.size <= 0 && !GetNextAudioPacket())
        return -1;
        
    while (data_size <= 0)
    {
        data_size = sizeof(sdata->buffer);
        len1 = avcodec_decode_audio3(audio_stream->codec,(int16_t *)sdata->buffer, &data_size,&sdata->tmp_pkt);
    
        if (len1 < 0)
        {    
            if (!GetNextAudioPacket())
                return -1;
            else
                continue;
        }        
        sdata->tmp_pkt.data += len1;
        sdata->tmp_pkt.size -= len1;                  
    }       
    
    pts = sdata->clock;
    *out_pts = pts;
    n = 2 * audio_stream->codec->channels;
    sdata->clock += (double)data_size /(double)(n * audio_stream->codec->sample_rate);
    
        /* We have data, return it and come back for more later */
    return data_size;
}

int Decoder::SynchronizeAudio(int samples_size,double pts)
{
	int n;
	double ref_clock;
    AudioStreamData *sdata = &this->audio_stream_data;
    int16_t* samples = (int16_t*)sdata->buffer;
	
	n = 2 * audio_stream->codec->channels;
	
	if(av_sync_type != SYNC_AUDIO_MASTER) {
		double diff, avg_diff;
		int wanted_size, min_size, max_size;
		
		ref_clock = GetMasterClock();
		diff = GetAudioClock() - ref_clock;
		
		if(diff < AV_NOSYNC_THRESHOLD) {
			// accumulate the diffs
			sdata->diff_cum = diff + sdata->diff_avg_coef * sdata->diff_cum;
			
			if(sdata->diff_avg_count < AUDIO_DIFF_AVG_NB) {
				sdata->diff_avg_count++;
			} else {
				avg_diff = sdata->diff_cum * (1.0 - sdata->diff_avg_coef);
				if(fabs(avg_diff) >= sdata->diff_threshold) 				{
					wanted_size = samples_size + ((int)(diff * audio_stream->codec->sample_rate) * n);
					min_size = samples_size * ((100 - SAMPLE_CORRECTION_PERCENT_MAX) / 100);
					max_size = samples_size * ((100 + SAMPLE_CORRECTION_PERCENT_MAX) / 100);
					if(wanted_size < min_size) {
						wanted_size = min_size;
					} else if (wanted_size > max_size) {
						wanted_size = max_size;
					}
					if(wanted_size < samples_size) {
						samples_size = wanted_size;
					} else if(wanted_size > samples_size) {
						uint8_t *samples_end, *q;
						int nb;
						
						nb = (samples_size - wanted_size);
						samples_end = (uint8_t *)samples + samples_size - n;
						q = samples_end + n;
						while(nb > 0) {
							memcpy(q, samples_end, n);
							q += n;
							nb -= n;
						}
						samples_size = wanted_size;
					}
				}
			}
		} else {
			sdata->diff_avg_count = 0;
			sdata->diff_cum = 0;
		}
	}
	return samples_size;
}

bool Decoder::GetNextAudioPacket()
{
    AudioStreamData *sdata = &this->audio_stream_data;
    
    if(sdata->last_pkt.data)
        av_free_packet(&sdata->last_pkt);
    
    if (audio_queue->Get(&sdata->last_pkt,1) < 0)
        return false;
    
    if(sdata->last_pkt.data == (uint8_t*)flush_str)
    {
        avcodec_flush_buffers(audio_stream->codec);			
        av_free_packet(&sdata->last_pkt);            
        if (audio_queue->Get(&sdata->last_pkt,1) < 0)
            return false;
    }   
    
    sdata->tmp_pkt.data = sdata->last_pkt.data;
    sdata->tmp_pkt.size = sdata->last_pkt.size;    
    
    if(sdata->last_pkt.pts != AV_NOPTS_VALUE)
        sdata->clock = av_q2d(audio_stream->time_base)*sdata->last_pkt.pts;
    
    return true;
}

double Decoder::GetAudioClock()
{
	double pts;
	int hw_buf_size, bytes_per_sec, n;
    AudioStreamData *sdata = &this->audio_stream_data;
	
	pts = sdata->clock; /* maintained in the audio thread */
	hw_buf_size = sdata->buf_size - sdata->buf_index;
	bytes_per_sec = 0;
	n = audio_stream->codec->channels * 2;
	if(audio_stream) {
		bytes_per_sec = audio_stream->codec->sample_rate * n;
	}
	if(bytes_per_sec) {
		pts -= (double)hw_buf_size / bytes_per_sec;
	}
	return pts;
}

double Decoder::GetVideoClock() 
{
	double delta;
	
/*	delta = (av_gettime() - video_current_pts_time) / 1000000.0;
	return video_current_pts + delta;*/
    
    return 0;
}

double Decoder::GetExternalClock() 
{
	return av_gettime() / 1000000.0;
}

double Decoder::GetMasterClock() 
{
	if(av_sync_type == SYNC_VIDEO_MASTER) {
		return GetVideoClock();
	} else if(av_sync_type == SYNC_AUDIO_MASTER) {
		return GetAudioClock();
	} else {
		return GetExternalClock();
	}
}

int Decoder::VideoGetBuffer(struct AVCodecContext *c, AVFrame *pic) 
{
	int ret = avcodec_default_get_buffer(c, pic);
	uint64_t *pts = (uint64_t*)av_malloc(sizeof(uint64_t));
	Decoder *decoder = (Decoder *)c->opaque;
	
	*pts = decoder->video_stream_data.pkt_pts;
	pic->opaque = pts;
    
	return ret;
}

void Decoder::VideoReleaseBuffer(struct AVCodecContext *c, AVFrame *pic)
{
	if(pic) 
        av_freep(&pic->opaque);
	avcodec_default_release_buffer(c, pic);
}

int Decoder::VideoThread(void *arg)
{
    Decoder             *decoder = (Decoder*)arg;
    AVPacket            packet;
    VideoStreamData     *sdata = &decoder->video_stream_data;
    double              pts;
    int                 len1,finished;
        
    while (true)
    {
        if (decoder->video_queue->Get(&packet,1) < 1)
            break;
        
        if (packet.data == (uint8_t*)flush_str)
        {
            avcodec_flush_buffers(decoder->video_stream->codec);
			continue;
        }
        
        pts = 0;
		
		// Save global pts to be stored in pFrame in first call
		sdata->pkt_pts = packet.pts;
		// Decode video frame
		
		len1 = avcodec_decode_video2(decoder->video_stream->codec, sdata->frame, &finished,&packet);
        
		if      (packet.dts == AV_NOPTS_VALUE && sdata->frame->opaque && *(uint64_t*)sdata->frame->opaque != AV_NOPTS_VALUE)
            pts = *(uint64_t *)sdata->frame->opaque;
		else if (packet.dts != AV_NOPTS_VALUE)
			pts = packet.dts;
		else
			pts = 0;
		
		pts *= av_q2d(decoder->video_stream->time_base);
		
		// Did we get a video frame?
		if(finished) 
        {
			pts = decoder->SynchronizeVideo(pts);
			if(decoder->QueuePicture(pts) < 0)
				break;
		}
		av_free_packet(&packet);
    }
    
    return 0;
}

double Decoder::SynchronizeVideo(double pts)
{
	double frame_delay;
    VideoStreamData *sdata = &video_stream_data;
	
	if(pts != 0)    /* if we have pts, set video clock to it */
		sdata->clock = pts;
	else /* if we aren't given a pts, set it to the clock */
		pts = sdata->clock;
    
	/* update the video clock */
	frame_delay = av_q2d(video_stream->codec->time_base);
	/* if we are repeating a frame, adjust clock accordingly */
	frame_delay  += sdata->frame->repeat_pict * (frame_delay * 0.5);
	sdata->clock += frame_delay;
    
	return pts;
}
