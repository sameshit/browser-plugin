/*
 *  VideoDecoder.h
 *  wvp
 *
 *  Created by macvoid on 20.07.11.
 *  Copyright 2011 __MyCompanyName__. All rights reserved.
 *
 */
#ifndef VIDEODECODER_H
#define VIDEODECODER_H
extern "C" {

#include <libavcodec/avcodec.h>
#include <libswscale/swscale.h>
#include <libavutil/avstring.h>
}

#include "PacketQueue.h"

//#ifdef __MINGW32__
//#undef main /* Prevents SDL from overriding main() */
//#endif
#include <stdio.h>
#include <math.h>

//#define SCREEN_TEST 


#define SDL_AUDIO_BUFFER_SIZE 1024
#define MAX_AUDIOQ_SIZE (5 * 16 * 1024)
#define MAX_VIDEOQ_SIZE (5 * 256 * 1024)
#define AV_SYNC_THRESHOLD 0.01
#define AV_NOSYNC_THRESHOLD 10.0
#define SAMPLE_CORRECTION_PERCENT_MAX 10
#define AUDIO_DIFF_AVG_NB 20
#define VIDEO_PICTURE_QUEUE_SIZE 1
#define DEFAULT_AV_SYNC_TYPE AV_SYNC_VIDEO_MASTER

enum {
	AV_SYNC_AUDIO_MASTER,
	AV_SYNC_VIDEO_MASTER,
	AV_SYNC_EXTERNAL_MASTER,
};
//#define TEST
typedef struct VideoPicture {
#ifdef SCREEN_TEST
	SDL_Overlay *bitmap;
#else	
	uint8_t	*bitmap;
#endif
#ifdef TEST
	uint8_t *yuv_bitmap;
#endif
	
	uint32_t linesize;
	int width, height; /* source height & width */
	int allocated;
	double pts;
} VideoPicture;

typedef struct my_loop my_loop;


static int OurGetBuffer(struct AVCodecContext *c, AVFrame *pic) ;
static void OurReleaseBuffer(struct AVCodecContext *c, AVFrame *pic);

struct Mix_Chunk;

class VideoDecoder
{	
public:
	VideoDecoder();
	void Play();
	void Stop();
	void Pause();
	void UnPause();
	bool IsPaused();
	bool IsPlaying();
	void SetVolume(uint8_t vol);	

	~VideoDecoder();
	uint64_t		GetVideoPktPts();
	VideoPicture*	GetLastPicture();
	uint32_t		GetHeight();
	uint32_t		GetWidth();
	void			SetRenderer(void (*)(void*), void*);
	void			SetSize(uint32_t _w,uint32_t _h);	
	void (*Render)(void*);
	void *render_data;
private:	
	AVFormatContext *pFormatCtx;
	int             videoStream, audioStream;
	
	int             av_sync_type;
	double          external_clock; /* external clock base */
	int64_t         external_clock_time;
	int             seek_req;
	int             seek_flags;
	int64_t         seek_pos;
	
	double          audio_clock;
	AVStream        *audio_st;
	PacketQueue     *audioq;
	uint8_t         audio_buf[(AVCODEC_MAX_AUDIO_FRAME_SIZE * 3) / 2];
	unsigned int    audio_buf_size;
	unsigned int    audio_buf_index;
	AVPacket        audio_pkt;
	uint8_t         *audio_pkt_data;
	int             audio_pkt_size;
	int             audio_hw_buf_size;
	double          audio_diff_cum; /* used for AV difference average computation */
	double          audio_diff_avg_coef;
	double          audio_diff_threshold;
	int             audio_diff_avg_count;
	double          frame_timer;
	double          frame_last_pts;
	double          frame_last_delay;
	double          video_clock; ///<pts of last decoded frame / predicted pts of next decoded frame
	double          video_current_pts; ///<current displayed pts (different from video_clock if frame fifos are used)
	int64_t         video_current_pts_time;  ///<time (av_gettime) at which we updated video_current_pts - used to have running video pts
	AVStream        *video_st;
	PacketQueue     *videoq;
	VideoPicture    pictq[VIDEO_PICTURE_QUEUE_SIZE];
	int             pictq_size, pictq_rindex, pictq_windex;
	SDL_mutex       *pictq_mutex;
	SDL_cond        *pictq_cond;
	SDL_Thread      *parse_tid;
	SDL_Thread      *video_tid;
	double          speed;
	
	char            filename[1024];
	int             quit;
	my_loop			*loop;
	uint32_t		h;
	uint32_t		w;
	AVPacket		flush_pkt;
	SDL_Surface		*screen;
	uint8_t			audio_buffer[1024];	
	Mix_Chunk		*chunk_mov;
	int				mix_movie_channel;
	bool			playing;
	bool			paused;
	SDL_TimerID		timer_id;
	
	int				__id;
	
	uint64_t		video_pkt_pts;
	
	double			GetAudioClock();
	double			GetVideoClock() ;
	double			GetExternalClock(); 
	double			GetMasterClock() ;
	int				SynchronizeAudio(short *samples, int samples_size, double pts) 	;
	int				AudioDecodeFrame(uint8_t *audio_buf, int buf_size, double *pts_ptr) 	;
	static void		AudioCallback(void *userdata, Uint8 *stream, int len) ;
	static Uint32	SDLRefreshTimer_cb(Uint32 interval,void *opaque) ;
	void			ScheduleRefresh(int delay) ;
	void			VideoDisplay();
	static void		VideoRefreshTimer(void *userdata) 	;
	static void		AllocPicture(void *userdata) ;
	int				QueuePicture(AVFrame *pFrame, double pts) ;
	double			SynchronizeVideo(AVFrame *src_frame, double pts) ;
	static int		VideoThread(void *arg);
	int				StreamComponentOpen(int stream_index) ;
	static int		DecodeInterrupt_cb(void) ;
	static int		DecodeThread(void *arg) 	;
	void			StreamSeek(int64_t pos, int rel) ;
	static void		MixerCallback(int chan, void * stream, int len, void * udata);
};
#endif