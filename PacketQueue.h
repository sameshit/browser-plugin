/*
 *  PacketQueue.h
 *  wvp
 *
 *  Created by macvoid on 20.07.11.
 *  Copyright 2011 __MyCompanyName__. All rights reserved.
 *
 */
#ifndef PACKETQUEUE_H
#define PACKETQUEUE_H
extern "C" {
#include <libavformat/avformat.h>
}
#include <SDL/SDL.h>
#include <SDL/SDL_thread.h>

class PacketQueue
{	
public:	
	PacketQueue(AVPacket *);
	~PacketQueue();	
	int Put(AVPacket *);
	int Get(AVPacket *,int);
	int Size();
	void Flush();
private:
	AVPacketList *first_pkt, *last_pkt;
	int nb_packets;
	int size;
	SDL_mutex *mutex;
	SDL_cond *cond;
	AVPacket *flush_pkt;
};
#endif