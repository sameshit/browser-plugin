//
//  Decoder.h
//  FireBreath
//
//  Created by Oleg on 11.08.11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#ifndef FireBreath_Decoder_h
#define FireBreath_Decoder_h
extern "C" {
    
#include <libavcodec/avcodec.h>
#include <libswscale/swscale.h>
#include <libavutil/avstring.h>
#include <libavformat/avformat.h>
}
#include <stdint.h>
#include <SDL_mixer/SDL_mixer.h>
#include <SDL/SDL_thread.h>
#include "PacketQueue.h"

#define MAX_AUDIOQ_SIZE (5 * 16 * 1024)
#define MAX_VIDEOQ_SIZE (5 * 256 * 1024)

#define SYNC_AUDIO_MASTER 1
#define SYNC_VIDEO_MASTER 2

#define AV_NOSYNC_THRESHOLD 10.0
#define SAMPLE_CORRECTION_PERCENT_MAX 10
#define AUDIO_DIFF_AVG_NB 20

static const char *flush_str = "FLUSH";
static AVPacket flush_pkt;

typedef struct AudioStreamData
{
    uint32_t            buf_size;
    uint32_t            buf_index;   
    AVPacket            last_pkt;
    AVPacket            tmp_pkt;
    double              clock;
    DECLARE_ALIGNED(16,uint8_t,buffer)[(AVCODEC_MAX_AUDIO_FRAME_SIZE * 3) / 2];
    double              diff_cum;
    double              diff_avg_coef;
	double              diff_threshold;
	int                 diff_avg_count;
}AudioStreamData;

typedef struct VideoStreamData
{
    double              frame_timer;
    double              frame_last_delay;
    int64_t             pts;
    uint64_t            pkt_pts;
    SDL_Thread          *thread_id;
    AVFrame             *frame;
    double              clock;
}VideoStreamData;


class Decoder
{
public:    
    Decoder();
    virtual ~Decoder(); 
    bool OpenFile(const char *);    
    void StartDecoding();
private:        
    bool                OpenAudioStream();
    bool                OpenVideoStream();    
    double              GetVideoClock();
    double              GetAudioClock();
    double              GetExternalClock();
    double              GetMasterClock();
    int                 DecodeAudioFrame(double *);
    int                 SynchronizeAudio(int,double);
    double              SynchronizeVideo(double);
    virtual int         QueuePicture(double) = 0;
    
    static int          ReadThread(void *); 
    static void         AudioCallback(int, void *, int , void *);
    
    static int          VideoGetBuffer(struct AVCodecContext *c, AVFrame *pic);
    static void         VideoReleaseBuffer(struct AVCodecContext *c, AVFrame *pic);
    
    static int          VideoThread(void *);    
    
    bool GetNextAudioPacket();
    
    AVFormatContext     *format_ctx;
    AVStream            *video_stream;
    AVStream            *audio_stream;
    int                 video_index;
    int                 audio_index;
    Mix_Chunk           *mix_chunk;
    int                 mix_channel;
    AudioStreamData     audio_stream_data;
    SDL_Thread          *read_thread_id;
    PacketQueue         *audio_queue;
    PacketQueue         *video_queue;
    uint8_t             audio_buffer[1024];
    int                 av_sync_type;
    SDL_mutex           *audio_exit_mutex;
    bool                exit;
    
protected:
    VideoStreamData     video_stream_data;
};

#endif
