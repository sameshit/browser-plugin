#/**********************************************************\ 
#
# Auto-Generated Plugin Configuration file
# for WebVision Player
#
#\**********************************************************/

set(PLUGIN_NAME "wvplayer")
set(PLUGIN_PREFIX "webvi")
set(COMPANY_NAME "WebVision")

# ActiveX constants:
set(FBTYPELIB_NAME wvplayerLib)
set(FBTYPELIB_DESC "wvplayer 1.0 Type Library")
set(IFBControl_DESC "wvplayer Control Interface")
set(FBControl_DESC "wvplayer Control Class")
set(IFBComJavascriptObject_DESC "wvplayer IComJavascriptObject Interface")
set(FBComJavascriptObject_DESC "wvplayer ComJavascriptObject Class")
set(IFBComEventSource_DESC "wvplayer IFBComEventSource Interface")
set(AXVERSION_NUM "1")

# NOTE: THESE GUIDS *MUST* BE UNIQUE TO YOUR PLUGIN/ACTIVEX CONTROL!  YES, ALL OF THEM!
set(FBTYPELIB_GUID ca557723-c551-5194-a9c5-e554d9f13f1b)
set(IFBControl_GUID 4ba43c31-8d50-56ce-a794-2d63a0cfb78f)
set(FBControl_GUID 351f3585-aa86-5d6f-8fb9-28252a445171)
set(IFBComJavascriptObject_GUID 5e806a82-f5a3-519a-87cf-c1543b02b1e5)
set(FBComJavascriptObject_GUID ea2c7499-2810-5853-a3f9-050a10e70b6e)
set(IFBComEventSource_GUID 2aa822d5-cb10-59f3-85b8-75e81e2b2e20)

# these are the pieces that are relevant to using it from Javascript
set(ACTIVEX_PROGID "WebVision.wvplayer")
set(MOZILLA_PLUGINID "webvision.tv/wvplayer")

# strings
set(FBSTRING_CompanyName "WebVision")
set(FBSTRING_FileDescription "WebVision player")
set(FBSTRING_PLUGIN_VERSION "1.0.0.0")
set(FBSTRING_LegalCopyright "Copyright 2011 WebVision")
set(FBSTRING_PluginFileName "np${PLUGIN_NAME}.dll")
set(FBSTRING_ProductName "WebVision Player")
set(FBSTRING_FileExtents "")
set(FBSTRING_PluginName "WebVision Player")
set(FBSTRING_MIMEType "application/x-wvplayer")

# Uncomment this next line if you're not planning on your plugin doing
# any drawing:

#set (FB_GUI_DISABLED 1)

# Mac plugin settings. If your plugin does not draw, set these all to 0
set(FBMAC_USE_QUICKDRAW 0)
set(FBMAC_USE_CARBON 1)
set(FBMAC_USE_COCOA 1)
set(FBMAC_USE_COREGRAPHICS 0)
set(FBMAC_USE_COREANIMATION 1)
set(FBMAC_USE_INVALIDATINGCOREANIMATION 1)

# If you want to register per-machine on Windows, uncomment this line
#set (FB_ATLREG_MACHINEWIDE 1)
