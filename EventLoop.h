/*
 *  EventLoop.h
 *  wvp
 *
 *  Created by macvoid on 13.07.11.
 *  Copyright 2011 __MyCompanyName__. All rights reserved.
 *
 */
#ifndef EVLOOP_H
#define EVLOOP_H

#include <SDL/SDL.h>
#include <SDL/SDL_thread.h>

typedef void (*simple_cb)(void*);

struct my_event {
	simple_cb cb;
	void *param;
	
	my_event *next;
};

struct my_loop {
	my_event *first;
	my_event *last;
	
	SDL_mutex *mutex;
	SDL_Thread *ev_thread;
};

int loop_go(void *_loop)
{
	my_event *tmp_ev;
	my_loop *loop = (my_loop *)_loop;
	
	while (1)
	{
		if (loop->first != NULL)
		{
			SDL_LockMutex(loop->mutex);
			
			loop->first->cb(loop->first->param);
			tmp_ev = loop->first;			
			loop->first = tmp_ev->next;			
			if (loop->first == NULL)
				loop->last = NULL;
			delete tmp_ev;						
			SDL_UnlockMutex(loop->mutex);
		}
		
		
		SDL_Delay(10);
	}
	
	return 0;
}


void loop_init(my_loop *loop)
{
	loop->first = NULL;
	loop->last  = NULL;
	
	loop->mutex = SDL_CreateMutex();
	loop->ev_thread = SDL_CreateThread(loop_go,loop);
}

void loop_free(my_loop *loop)
{
	my_event *ev_tmp,*ev_prev = NULL;
	
	SDL_KillThread(loop->ev_thread);
	SDL_DestroyMutex(loop->mutex);
	
	for (ev_tmp = loop->first; ev_tmp != NULL; ev_prev = ev_tmp, ev_tmp = ev_tmp->next)
		if (ev_prev!=NULL)
			delete ev_prev;
	if (ev_prev != NULL)
		delete ev_prev;
		
}


void loop_push(my_loop *loop, simple_cb cb, void *param)
{	
	SDL_LockMutex(loop->mutex);
	if (loop->last == NULL)
	{
		loop->first = new my_event;		
		loop->last = loop->first;
	}
	else
	{
		loop->last->next = new my_event;
		loop->last = loop->last->next;
	}
	
	loop->last->next	= NULL;
	loop->last->cb		= cb;
	loop->last->param	= param;
	SDL_UnlockMutex(loop->mutex);
}

#endif

