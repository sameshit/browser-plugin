/*
 *  VideoDecoder.cpp
 *  wvp
 *
 *  Created by macvoid on 20.07.11.
 *  Copyright 2011 __MyCompanyName__. All rights reserved.
 *
 */
#include "VideoDecoder.h"
#include "EventLoop.h"
#include <SDL_mixer/SDL_mixer.h>
#include <assert.h>

VideoDecoder::VideoDecoder()
{
	memset(this,0,sizeof(VideoDecoder));
	video_pkt_pts = AV_NOPTS_VALUE;
	speed = 1;
	
	srand(time(NULL));
	__id = rand() % 10;
	
	parse_tid = NULL;
	video_tid = NULL;
	
	av_strlcpy(filename, "/Users/void/Downloads/test.flv", sizeof(filename));         
	
	pictq_mutex = SDL_CreateMutex();
	pictq_cond = SDL_CreateCond();
	loop = new my_loop;
	
	loop_init(loop);

#ifdef SCREEN_TEST
	screen = SDL_SetVideoMode(640, 480, 0, 0);
	 if(!screen) {
	 fprintf(stderr, "SDL: could not set video mode - exiting\n");
	 exit(1);
	 }
#endif
	 	
	 videoq = new PacketQueue(&flush_pkt);
	 audioq = new PacketQueue(&flush_pkt);
	 playing = false;
	 paused = false;
	
	 Render = NULL;
}

void VideoDecoder::Play()
{
	ScheduleRefresh(40);
	
	av_sync_type = DEFAULT_AV_SYNC_TYPE;
	assert(parse_tid == NULL);
	parse_tid = SDL_CreateThread(VideoDecoder::DecodeThread, this);
	
	if(!parse_tid) 
	{
		exit(1);
	}
	
	av_init_packet(&flush_pkt);
	flush_pkt.data = (uint8_t*)"FLUSH";
	
	playing = true;
}

bool VideoDecoder::IsPlaying()
{
	return playing;
}

void VideoDecoder::Stop()
{
	if (!playing)
		return;
	playing = false;
	
	Mix_HaltChannel(this->mix_movie_channel);
}

void VideoDecoder::Pause()
{
	

	Mix_Pause(this->mix_movie_channel);
	paused = true;
}

void VideoDecoder::UnPause()
{
	Mix_Resume(this->mix_movie_channel);
	paused = false;
}

bool VideoDecoder::IsPaused()
{
	return paused;
}

void VideoDecoder::SetSize(uint32_t _w, uint32_t _h)
{
	this->w = _w;
	this->h	= _h;
}

VideoDecoder::~VideoDecoder()
{
	if (parse_tid != NULL)
		SDL_KillThread(parse_tid);
	if (video_tid != NULL)
		SDL_KillThread(video_tid);	

	SDL_RemoveTimer(timer_id);
	
	if (playing)
		Stop();
	
	delete videoq;
	delete audioq;
	loop_free(loop);
	delete loop;
}

double VideoDecoder::GetAudioClock()
{
	double pts;
	int hw_buf_size, bytes_per_sec, n;
	
	pts = audio_clock; /* maintained in the audio thread */
	hw_buf_size = audio_buf_size - audio_buf_index;
	bytes_per_sec = 0;
	n = audio_st->codec->channels * 2;
	if(audio_st) {
		bytes_per_sec = audio_st->codec->sample_rate * n;
	}
	if(bytes_per_sec) {
		pts -= (double)hw_buf_size / bytes_per_sec;
	}
	return pts;
}

double VideoDecoder::GetVideoClock() 
{
	double delta;
	
	delta = (av_gettime() - video_current_pts_time) / 1000000.0;
	return video_current_pts + delta;
}

double VideoDecoder::GetExternalClock() 
{
	return av_gettime() / 1000000.0;
}

double VideoDecoder::GetMasterClock() 
{
	if(av_sync_type == AV_SYNC_VIDEO_MASTER) {
		return GetVideoClock();
	} else if(av_sync_type == AV_SYNC_AUDIO_MASTER) {
		return GetAudioClock();
	} else {
		return GetExternalClock();
	}
}

/* Add or subtract samples to get a better sync, return new
 audio buffer size */

int VideoDecoder::SynchronizeAudio(short *samples, int samples_size, double pts) 
{
	int n;
	double ref_clock;
	
	n = 2 * audio_st->codec->channels;
	
	if(av_sync_type != AV_SYNC_AUDIO_MASTER) {
		double diff, avg_diff;
		int wanted_size, min_size, max_size, nb_samples;
		
		ref_clock = GetMasterClock();
		diff = GetAudioClock() - ref_clock;
		
		if(diff < AV_NOSYNC_THRESHOLD) {
			// accumulate the diffs
			audio_diff_cum = diff + audio_diff_avg_coef * audio_diff_cum;
			
			if(audio_diff_avg_count < AUDIO_DIFF_AVG_NB) {
				audio_diff_avg_count++;
			} else {
				avg_diff = audio_diff_cum * (1.0 - audio_diff_avg_coef);
				if(fabs(avg_diff) >= audio_diff_threshold) 				{
					wanted_size = samples_size + ((int)(diff * audio_st->codec->sample_rate) * n);
					min_size = samples_size * ((100 - SAMPLE_CORRECTION_PERCENT_MAX) / 100);
					max_size = samples_size * ((100 + SAMPLE_CORRECTION_PERCENT_MAX) / 100);
					if(wanted_size < min_size) {
						wanted_size = min_size;
					} else if (wanted_size > max_size) {
						wanted_size = max_size;
					}
					if(wanted_size < samples_size) {
						/* remove samples */
						samples_size = wanted_size;
					} else if(wanted_size > samples_size) {
						uint8_t *samples_end, *q;
						int nb;
						
						/* add samples by copying final sample*/
						nb = (samples_size - wanted_size);
						samples_end = (uint8_t *)samples + samples_size - n;
						q = samples_end + n;
						while(nb > 0) {
							memcpy(q, samples_end, n);
							q += n;
							nb -= n;
						}
						samples_size = wanted_size;
					}
				}
			}
		} else {
			/* difference is TOO big; reset diff stuff */
			audio_diff_avg_count = 0;
			audio_diff_cum = 0;
		}
	}
	return speed*samples_size;
}

int VideoDecoder::AudioDecodeFrame(uint8_t *audio_buf, int buf_size, double *pts_ptr) 
{	
	int len1, data_size, n;
	AVPacket *pkt = &audio_pkt,pkt1;
	double pts;
	
	for(;;) {
		while(audio_pkt_size > 0) {
			data_size = buf_size;
			pkt1.data = audio_pkt_data;
			pkt1.size = audio_pkt_size;
			len1 = avcodec_decode_audio3(audio_st->codec,
										 (int16_t *)audio_buf, &data_size,
										 &pkt1);
			if(len1 < 0) {
				/* if error, skip frame */
				audio_pkt_size = 0;
				break;
			}
			audio_pkt_data += len1;
			audio_pkt_size -= len1;
			if(data_size <= 0) {
				/* No data yet, get more frames */
				continue;
			}
			pts = audio_clock;
			*pts_ptr = pts;
			n = 2 * audio_st->codec->channels;
			audio_clock += (double)data_size /
			(double)(n * audio_st->codec->sample_rate);
			
			/* We have data, return it and come back for more later */
			return data_size;
		}
		if(pkt->data)
			av_free_packet(pkt);
		
		if(quit) {
			return -1;
		}
		/* next packet */
		if(audioq->Get(pkt,1) < 0) {
			return -1;
		}
		if(pkt->data == flush_pkt.data) {
			avcodec_flush_buffers(audio_st->codec);
			continue;
		}
		audio_pkt_data = pkt->data;
		audio_pkt_size = pkt->size;
		/* if update, update the audio clock w/pts */
		if(pkt->pts != AV_NOPTS_VALUE) {
			audio_clock = av_q2d(audio_st->time_base)*pkt->pts;
		}
	}
}

void VideoDecoder::AudioCallback(void *userdata, Uint8 *stream, int len) 
{
	
	VideoDecoder *is = (VideoDecoder *)userdata;
	int len1, audio_size;
	double pts;
	
	while(len > 0) {
		if(is->audio_buf_index >= is->audio_buf_size) {
			/* We have already sent all our data; get more */
			
			audio_size = is->AudioDecodeFrame(is->audio_buf, sizeof(is->audio_buf), &pts);
			if(audio_size < 0) {
				/* If error, output silence */
				is->audio_buf_size = 1024;
				memset(is->audio_buf, 0, is->audio_buf_size);
			} else {
				audio_size = is->SynchronizeAudio((int16_t *)is->audio_buf,
											   audio_size, pts);
				is->audio_buf_size = audio_size;
			}
			is->audio_buf_index = 0;
		}
		len1 = is->audio_buf_size - is->audio_buf_index;
		if(len1 > len)
			len1 = len;
		memcpy(stream, (uint8_t *)is->audio_buf + is->audio_buf_index, len1);
		len -= len1;
		stream += len1;
		is->audio_buf_index += len1;
	}
}

Uint32 VideoDecoder::SDLRefreshTimer_cb(Uint32 interval,void *opaque) 
{	
	VideoDecoder *is = (VideoDecoder*) opaque;

	loop_push(is->loop, VideoDecoder::VideoRefreshTimer, is)	;
	return 0; /* 0 means stop timer */
}

/* schedule a video refresh in 'delay' ms */
void VideoDecoder::ScheduleRefresh(int delay) 
{
	timer_id = SDL_AddTimer(delay, VideoDecoder::SDLRefreshTimer_cb, this);
}

void VideoDecoder::VideoDisplay() 
{	
	SDL_Rect rect;
	VideoPicture *vp;
	AVPicture pict;
	float aspect_ratio;
	int _w, _h, _x, _y;
	int i;
	
	vp = &pictq[pictq_rindex];
	if(vp->bitmap) {
		if(video_st->codec->sample_aspect_ratio.num == 0) {
			aspect_ratio = 0;
		} else {
			aspect_ratio = av_q2d(video_st->codec->sample_aspect_ratio) *
			video_st->codec->width / video_st->codec->height;
		}
		if(aspect_ratio <= 0.0) {
			aspect_ratio = (float)video_st->codec->width /
			(float)video_st->codec->height;
		}
		_h = this->h;
		_w = ((int)rint(_h * aspect_ratio)) & -3;
		if(_w > this->w) {
			_w = this->w;
			_h = ((int)rint(w / aspect_ratio)) & -3;
		}
		_x = (this->w - _w) / 2;
		_y = (this->h - _h) / 2;
		
		rect.x = _x;
		rect.y = _y;
		rect.w = _w;
		rect.h = _h;
		

#ifdef SCREEN_TEST	
	    SDL_DisplayYUVOverlay(vp->bitmap, &rect);
#endif
	}
}

void VideoDecoder::VideoRefreshTimer(void *userdata) 
{	
	VideoDecoder *is = (VideoDecoder *)userdata;
	VideoPicture *vp;
	double actual_delay, delay, sync_threshold, ref_clock, diff;
	
	if(is->video_st) {
		if(is->pictq_size == 0) {
			is->ScheduleRefresh(1);
		} else {
			vp = &is->pictq[is->pictq_rindex];
			
			is->video_current_pts = vp->pts;
			is->video_current_pts_time = av_gettime();
			
			delay = vp->pts - is->frame_last_pts; /* the pts from last time */
			if(delay <= 0 || delay >= 1.0) {
				/* if incorrect delay, use previous one */
				delay = is->frame_last_delay;
			}
			/* save for next time */
			is->frame_last_delay = delay;
			is->frame_last_pts = vp->pts;
			
			/* update delay to sync to audio if not master source */
			if(is->av_sync_type != AV_SYNC_VIDEO_MASTER) {
				ref_clock = is->GetMasterClock();
				diff = vp->pts - ref_clock;
				
				/* Skip or repeat the frame. Take delay into account
				 FFPlay still doesn't "know if this is the best guess." */
				sync_threshold = (delay > AV_SYNC_THRESHOLD) ? delay : AV_SYNC_THRESHOLD;
				if(fabs(diff) < AV_NOSYNC_THRESHOLD) {
					if(diff <= -sync_threshold) {
						delay = 0;
					} else if(diff >= sync_threshold) {
						delay = 2 * delay;
					}
				}
			}
			
			is->frame_timer += delay;
			/* computer the REAL delay */
			actual_delay = is->frame_timer - (av_gettime() / 1000000.0);
			if(actual_delay < 0.010) {
				/* Really it should skip the picture instead */
				actual_delay = 0.010;
			}
			is->ScheduleRefresh((int)(actual_delay * 1000 + 0.5));
			
			/* show the picture! */
#ifdef SCREEN_TEST			
			is->VideoDisplay();
#else
			if (is->Render != NULL)
				is->Render(is->render_data);
#endif	
			
			/* update queue for next picture! */
			if(++is->pictq_rindex == VIDEO_PICTURE_QUEUE_SIZE) {
				is->pictq_rindex = 0;
			}
			SDL_LockMutex(is->pictq_mutex);
			is->pictq_size--;
			SDL_CondSignal(is->pictq_cond);
			SDL_UnlockMutex(is->pictq_mutex);
		}
	} else {
		is->ScheduleRefresh(100);
	}
}

void VideoDecoder::AllocPicture(void *userdata) {
	
	VideoDecoder *is = (VideoDecoder *)userdata;
	VideoPicture *vp;
	
	vp = &is->pictq[is->pictq_windex];
	if(vp->bitmap) {
		// we already have one make another, bigger/smaller
#ifdef SCREEN_TEST
		   SDL_FreeYUVOverlay(vp->bitmap);
#else
		   av_free(vp->bitmap);
		   vp->bitmap = NULL;
#ifdef TEST
		av_free(vp->yuv_bitmap);
		vp->yuv_bitmap = NULL;
#endif
#endif
	}
	// Allocate a place to put our YUV image on that screen
#ifdef SCREEN_TEST
	 vp->bitmap = SDL_CreateYUVOverlay(is->video_st->codec->width,	 is->video_st->codec->height,	 SDL_YV12_OVERLAY,	 is->screen);
#else
	 vp->bitmap = (uint8_t*)av_malloc(is->w*is->h*2);
#ifdef TEST
	 vp->yuv_bitmap = (uint8_t*)av_malloc(is->w*is->h*3);
#endif
#endif
	
	vp->width = is->video_st->codec->width;
	vp->height = is->video_st->codec->height;
	
	SDL_LockMutex(is->pictq_mutex);
	vp->allocated = 1;
	SDL_CondSignal(is->pictq_cond);
	SDL_UnlockMutex(is->pictq_mutex);
	
}

int VideoDecoder::QueuePicture(AVFrame *pFrame, double pts) 
{
	
	VideoPicture *vp;
	AVPicture pict;
	
	/* wait until we have space for a new pic */
	SDL_LockMutex(pictq_mutex);
	while(pictq_size >= VIDEO_PICTURE_QUEUE_SIZE &&
		  !quit) {
		SDL_CondWait(pictq_cond, pictq_mutex);
	}
	SDL_UnlockMutex(pictq_mutex);
	
	if(quit)
		return -1;
	
	// windex is set to 0 initially
	vp = &pictq[pictq_windex];
	
	/* allocate or resize the buffer! */
	if(!vp->bitmap ||
	   vp->width != video_st->codec->width ||
	   vp->height !=video_st->codec->height) {
		//    SDL_Event event;
		
		vp->allocated = 0;
		/* we have to do it in the main thread */
		//    event.type = FF_ALLOC_EVENT;
		//    event.user.data1 = is;
		//    SDL_PushEvent(&event);
		
		loop_push(loop, VideoDecoder::AllocPicture, this);
		
		/* wait until we have a picture allocated */
		SDL_LockMutex(pictq_mutex);
		while(!vp->allocated && !quit) {
			SDL_CondWait(pictq_cond, pictq_mutex);
		}
		SDL_UnlockMutex(pictq_mutex);
		if(quit)
		{
			return -1;
		}
	}
	/* We have a place to put our picture on the queue */
	/* If we are skipping a frame, do we set this to null
     but still return vp->allocated = 1? */
	
	
	if(vp->bitmap) {
		
#ifdef SCREEN_TEST
		SDL_LockYUVOverlay(vp->bitmap);

		
		/* point pict at the queue */
		
		 pict.data[0] = vp->bitmap->pixels[0];
		 pict.data[1] = vp->bitmap->pixels[2];
		 pict.data[2] = vp->bitmap->pixels[1];
		 
		 pict.linesize[0] = vp->bitmap->pitches[0];
		 pict.linesize[1] = vp->bitmap->pitches[2];
		 pict.linesize[2] = vp->bitmap->pitches[1];
#else 
		pict.data[0] = &vp->bitmap[0]; 
		pict.data[1] = &vp->bitmap[h*w/2];
		pict.data[2] = &vp->bitmap[h*w];
		pict.data[3] = &vp->bitmap[3*h*w/2];
		
		pict.linesize[0] = 2*w;
		pict.linesize[1] = 2*w;
		pict.linesize[2] = 2*w;
		pict.linesize[3] = 2*w;

#endif

		// Convert the image into YUV format that SDL uses
		struct SwsContext *t;
		int src_w = video_st->codec->width;
		int src_h = video_st->codec->height;
		t = sws_getContext(src_w,src_h,video_st->codec->pix_fmt,w,h,PIX_FMT_UYVY422,SWS_BICUBIC,NULL,NULL,NULL);

		sws_scale(t,pFrame->data,pFrame->linesize,0,src_h,(uint8_t* const*)pict.data,(const int*)pict.linesize);
		
		/*img_convert(&pict, dst_pix_fmt,
		 (AVPicture *)pFrame, is->video_st->codec->pix_fmt,
		 is->video_st->codec->width, is->video_st->codec->height);*/
		
#ifdef SCREEN_TEST
		SDL_UnlockYUVOverlay(vp->bitmap);
#endif
		vp->pts = pts;
		
		/* now we inform our display thread that we have a pic ready */
		if(++pictq_windex == VIDEO_PICTURE_QUEUE_SIZE) {
			pictq_windex = 0;
		}
		sws_freeContext(t);
		SDL_LockMutex(pictq_mutex);
		pictq_size++;
		SDL_UnlockMutex(pictq_mutex);
	}
	return 0;
}

double VideoDecoder::SynchronizeVideo(AVFrame *src_frame, double pts) 
{	
	double frame_delay;
	
	if(pts != 0) {
		/* if we have pts, set video clock to it */
		video_clock = pts;
	} else {
		/* if we aren't given a pts, set it to the clock */
		pts = video_clock;
	}
	/* update the video clock */
	frame_delay = av_q2d(video_st->codec->time_base);
	/* if we are repeating a frame, adjust clock accordingly */
	frame_delay += src_frame->repeat_pict * (frame_delay * 0.5);
	video_clock += frame_delay;
	return speed*pts;
}



/* These are called whenever we allocate a frame
 * buffer. We use this to store the global_pts in
 * a frame at the time it is allocated.
 */

int VideoDecoder::VideoThread(void *arg)
{
	VideoDecoder *is = (VideoDecoder *)arg;
	AVPacket pkt1, *packet = &pkt1,pkt;
	int len1, frameFinished;
	AVFrame *pFrame;
	double pts;
	
	pFrame = avcodec_alloc_frame();
	
	for(;;) {
		if (is->IsPaused())
		{
			SDL_Delay(10);
			continue;
		}
		
		if(is->videoq->Get(packet, 1) < 0) {
			// means we quit getting packets
			break;
		}
		if(packet->data == is->flush_pkt.data) {
			avcodec_flush_buffers(is->video_st->codec);
			continue;
		}
		pts = 0;
		
		// Save global pts to be stored in pFrame in first call
		is->video_pkt_pts = packet->pts;
		// Decode video frame
		
		len1 = avcodec_decode_video2(is->video_st->codec, pFrame, &frameFinished,
									 packet);
		if(packet->dts == AV_NOPTS_VALUE
		   && pFrame->opaque && *(uint64_t*)pFrame->opaque != AV_NOPTS_VALUE) {
			pts = *(uint64_t *)pFrame->opaque;
		} else if(packet->dts != AV_NOPTS_VALUE) {
			pts = packet->dts;
		} else {
			pts = 0;
		}
		pts *= av_q2d(is->video_st->time_base);
		
		// Did we get a video frame?
		if(frameFinished) {
			pts = is->SynchronizeVideo(pFrame, pts);
			if(is->QueuePicture(pFrame, pts) < 0) {
				break;
			}
		}
		av_free_packet(packet);
	}
	av_free(pFrame);
	return 0;
}

void VideoDecoder::MixerCallback(int chan, void * _stream, int len, void * udata)
{
	
	VideoDecoder *is = (VideoDecoder *)udata;
	uint8_t *stream = (uint8_t *)_stream;
	int len1, audio_size;
	double pts;
	
	while(len > 0) {
		if(is->audio_buf_index >= is->audio_buf_size) {
			/* We have already sent all our data; get more */
			
			audio_size = is->AudioDecodeFrame(is->audio_buf, sizeof(is->audio_buf), &pts);
			if(audio_size < 0) {
				/* If error, output silence */
				is->audio_buf_size = 1024;
				memset(is->audio_buf, 0, is->audio_buf_size);
			} else {
				audio_size = is->SynchronizeAudio((int16_t *)is->audio_buf,
												  audio_size, pts);
				is->audio_buf_size = audio_size;
			}
			is->audio_buf_index = 0;
		}
		len1 = is->audio_buf_size - is->audio_buf_index;
		if(len1 > len)
			len1 = len;
		memcpy(stream, (uint8_t *)is->audio_buf + is->audio_buf_index, len1);
		len -= len1;
		stream += len1;
		is->audio_buf_index += len1;
		
	}
//	VideoDecoder::AudioCallback(udata,(uint8_t*)stream,len);
//	old_sdl_audio_callback_that_used_ffmpeg_data(udata, stream, len);
}
int VideoDecoder::StreamComponentOpen(int stream_index) 
{	
	AVCodecContext *codecCtx;
	AVCodec *codec;
	SDL_AudioSpec wanted_spec, spec;
	
	if(stream_index < 0 || stream_index >= pFormatCtx->nb_streams) {
		return -1;
	}
	
	// Get a pointer to the codec context for the video stream
	codecCtx = pFormatCtx->streams[stream_index]->codec;
	
	if(codecCtx->codec_type == AVMEDIA_TYPE_AUDIO) {
		// Set audio settings from codec info
		wanted_spec.freq = codecCtx->sample_rate;
		wanted_spec.format = AUDIO_S16SYS;
		wanted_spec.channels = codecCtx->channels;
		wanted_spec.silence = 0;
		wanted_spec.samples = SDL_AUDIO_BUFFER_SIZE;
		wanted_spec.callback = VideoDecoder::AudioCallback;
		wanted_spec.userdata = this;
				
		char errorr[122];
				

		audio_hw_buf_size = 1024;
		chunk_mov = Mix_QuickLoad_RAW(audio_buffer, 1024);
		mix_movie_channel = Mix_PlayChannel(-1, chunk_mov, -1);
		Mix_RegisterEffect(mix_movie_channel, VideoDecoder::MixerCallback, NULL, this);
		


/*		if(SDL_OpenAudio(&wanted_spec, &spec) < 0) {
			sprintf(errorr, "SDL_OpenAudio: %s\n", SDL_GetError());
			return -1;
		}
		audio_hw_buf_size = spec.size;*/
	}
	codec = avcodec_find_decoder(codecCtx->codec_id);
	if(!codec || (avcodec_open(codecCtx, codec) < 0)) {
		fprintf(stderr, "Unsupported codec!\n");
		return -1;
	}
	
	switch(codecCtx->codec_type) {
		case AVMEDIA_TYPE_AUDIO:
			audioStream = stream_index;
			audio_st = pFormatCtx->streams[stream_index];
			audio_buf_size = 0;
			audio_buf_index = 0;
			
			/* averaging filter for audio sync */
			audio_diff_avg_coef = exp(log(0.01 / AUDIO_DIFF_AVG_NB));
			audio_diff_avg_count = 0;
			/* Correct audio only if larger error than this */
			audio_diff_threshold = 2.0 * SDL_AUDIO_BUFFER_SIZE / codecCtx->sample_rate;
			
//			memset(&audio_pkt, 0, sizeof(audio_pkt));
//			packet_queue_init(&audioq);
//			SDL_PauseAudio(0);
			break;
		case AVMEDIA_TYPE_VIDEO:
			videoStream = stream_index;
			video_st = pFormatCtx->streams[stream_index];
			
			frame_timer = (double)av_gettime() / 1000000.0;
			frame_last_delay = 40e-3;
			video_current_pts_time = av_gettime();
			
			video_tid = SDL_CreateThread(VideoDecoder::VideoThread, this);
			codecCtx->get_buffer = OurGetBuffer;
			codecCtx->release_buffer = OurReleaseBuffer;
			codecCtx->opaque = this;
			
			break;
		default:
			break;
	}
	
	
}

int VideoDecoder::DecodeInterrupt_cb(void) 
{
	return 1==0;//(this && this->quit); NEED TO DO SMTH WITH THIS
}
int VideoDecoder::DecodeThread(void *arg) 
{	
	VideoDecoder *is = (VideoDecoder *)arg;
	AVFormatContext *pFormatCtx;
	AVPacket pkt1, *packet = &pkt1;
	
	int video_index = -1;
	int audio_index = -1;
	int i;
	
	is->videoStream=-1;
	is->audioStream=-1;

	// will interrupt blocking functions if we quit!
	url_set_interrupt_cb(VideoDecoder::DecodeInterrupt_cb);
	
	// Open video file
	if(av_open_input_file(&pFormatCtx, is->filename, NULL, 0, NULL)!=0)
		return -1; // Couldn't open file
	
	is->pFormatCtx = pFormatCtx;
	
	// Retrieve stream information
	if(av_find_stream_info(pFormatCtx)<0)
		return -1; // Couldn't find stream information
	
	// Dump information about file onto standard error
	dump_format(pFormatCtx, 0, is->filename, 0);
	
	// Find the first video stream
	for(i=0; i<pFormatCtx->nb_streams; i++) {
		if(pFormatCtx->streams[i]->codec->codec_type==AVMEDIA_TYPE_VIDEO &&
		   video_index < 0) {
			video_index=i;
		}
		if(pFormatCtx->streams[i]->codec->codec_type==AVMEDIA_TYPE_AUDIO &&
		   audio_index < 0) {
			audio_index=i;
		}
	}
	if(audio_index >= 0) {
		is->StreamComponentOpen(audio_index);
	}
	if(video_index >= 0) {
		is->StreamComponentOpen(video_index);
	}
	
	if(is->videoStream < 0 || is->audioStream < 0) {
		fprintf(stderr, "%s: could not open codecs\n", is->filename);
		goto fail;
	}
	
	// main decode loop
	
	for(;;) {
		if(is->quit) {
			break;
		}
		if (is->IsPaused())
		{
			SDL_Delay(30);
			continue;
		}
		// seek stuff goes here
		if(is->seek_req) {
			int stream_index= -1;
			int64_t seek_target = is->seek_pos;
			
			if     (is->videoStream >= 0) stream_index = is->videoStream;
			else if(is->audioStream >= 0) stream_index = is->audioStream;
			
			if(stream_index>=0){
				seek_target= av_rescale_q(seek_target, AV_TIME_BASE_Q, pFormatCtx->streams[stream_index]->time_base);
			}
			if(!av_seek_frame(is->pFormatCtx, stream_index, seek_target, is->seek_flags)) {
				fprintf(stderr, "%s: error while seeking\n", is->pFormatCtx->filename);
			} else {
				if(is->audioStream >= 0) {
					is->audioq->Flush();
					is->audioq->Put(&is->flush_pkt);
				}
				if(is->videoStream >= 0) {
					is->videoq->Flush();
					is->videoq->Put(&is->flush_pkt);
				}
			}
			is->seek_req = 0;
		}
		
		if(is->audioq->Size() > MAX_AUDIOQ_SIZE ||
		   is->videoq->Size() > MAX_VIDEOQ_SIZE) {
			SDL_Delay(10);
			continue;
		}
		if(av_read_frame(is->pFormatCtx, packet) < 0) {
			if(url_ferror((ByteIOContext*)&pFormatCtx->pb) == 0) {
				SDL_Delay(100); /* no error; wait for user input */
				continue;
			} else {
				break;
			}
		}
		// Is this a packet from the video stream?
		if(packet->stream_index == is->videoStream) {
			is->videoq->Put(packet);
		} else if(packet->stream_index == is->audioStream) {
			
			is->audioq->Put(packet);
		} else {
			av_free_packet(packet);
		}
	}
	/* all done - wait for it */
	while(!is->quit) {
		SDL_Delay(100);
	}
fail:
	{
//		SDL_Event event;
//		event.type = FF_QUIT_EVENT;
//		event.user.data1 = is;
//		SDL_PushEvent(&event);
	}
	return 0;
}

void VideoDecoder::StreamSeek(int64_t pos, int rel) 
{	
	if(!seek_req) {
		seek_pos = pos;
		seek_flags = rel < 0 ? AVSEEK_FLAG_BACKWARD : 0;
		seek_req = 1;
	}
}

inline uint64_t VideoDecoder::GetVideoPktPts()
{
	return this->video_pkt_pts;
}

static int OurGetBuffer(struct AVCodecContext *c, AVFrame *pic) 
{
	int ret = avcodec_default_get_buffer(c, pic);
	uint64_t *pts = (uint64_t*)av_malloc(sizeof(uint64_t));
	VideoDecoder *is = (VideoDecoder *)c->opaque;
	
	*pts = is->GetVideoPktPts();
	pic->opaque = pts;
	return ret;
}

static void OurReleaseBuffer(struct AVCodecContext *c, AVFrame *pic)
{
	if(pic) av_freep(&pic->opaque);
	avcodec_default_release_buffer(c, pic);
}

VideoPicture* VideoDecoder::GetLastPicture()
{
	return &pictq[pictq_rindex];
}

uint32_t VideoDecoder::GetHeight()
{
	return video_st->codec->height;
}

uint32_t VideoDecoder::GetWidth()
{
	return video_st->codec->width;
}

void	VideoDecoder::SetRenderer(void (*_render)(void*), void* _render_data)
{
	this->Render = _render;
	this->render_data = _render_data;
}

void VideoDecoder::SetVolume(uint8_t volume)
{
	Mix_Volume(mix_movie_channel, volume);
}