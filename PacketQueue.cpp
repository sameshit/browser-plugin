/*
 *  PacketQueue.cpp
 *  wvp
 *
 *  Created by macvoid on 20.07.11.
 *  Copyright 2011 __MyCompanyName__. All rights reserved.
 *
 */

#include "PacketQueue.h"

PacketQueue::PacketQueue(AVPacket *_flush_pkt)
{
	first_pkt = NULL;
	last_pkt  = NULL;
	nb_packets = 0;
	size = 0;
	flush_pkt = _flush_pkt;
	
	mutex = SDL_CreateMutex();
	cond = SDL_CreateCond();
}

PacketQueue::~PacketQueue()
{
    Flush();	
	// ALSO SHOULD FREE PACKET LIST
}


int PacketQueue::Put(AVPacket *pkt)
{	
	AVPacketList *pkt1;
	
	if(pkt != flush_pkt && av_dup_packet(pkt) < 0) {
		return -1;
	}
	pkt1 = (AVPacketList*)av_malloc(sizeof(AVPacketList));
	if (!pkt1)
		return -1;
	pkt1->pkt = *pkt;
	pkt1->next = NULL;
	
	SDL_LockMutex(mutex);
	
	if (!last_pkt)
		first_pkt = pkt1;
	else
		last_pkt->next = pkt1;
	last_pkt = pkt1;
	nb_packets++;
	size += pkt1->pkt.size;
	SDL_CondSignal(cond);
	
	SDL_UnlockMutex(mutex);
	return 0;	
}

int PacketQueue::Get(AVPacket *pkt, int block)
{
	AVPacketList *pkt1;
	int ret;
	
	SDL_LockMutex(mutex);
	
	for(;;) {
		
/*		if(global_video_state->quit) 
		{
			ret = -1;
			break;
		}*/
		
		pkt1 = first_pkt;
		if (pkt1) {
			first_pkt = pkt1->next;
			if (!first_pkt)
				last_pkt = NULL;
			nb_packets--;
			size -= pkt1->pkt.size;
			*pkt = pkt1->pkt;
			av_free(pkt1);
			ret = 1;
			break;
		} else if (!block) {
			ret = 0;
			break;
		} else {
			SDL_CondWait(cond, mutex);
		}
	}
	SDL_UnlockMutex(mutex);
	return ret;
}

void PacketQueue::Flush() 
{
	AVPacketList *pkt, *pkt1;
	
	SDL_LockMutex(mutex);
	for(pkt = first_pkt; pkt != NULL; pkt = pkt1) {
		pkt1 = pkt->next;
		av_free_packet(&pkt->pkt);
		av_freep(&pkt);
	}
	last_pkt = NULL;
	first_pkt = NULL;
	nb_packets = 0;
	size = 0;
	SDL_UnlockMutex(mutex);
}

int PacketQueue::Size()
{
	return size;
}

